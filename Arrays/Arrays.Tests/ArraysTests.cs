using Xunit;
using Library;

namespace Arrays.Tests
{
    public class ArraysTests
    {
        [Fact]
        public void Test1()
        {
            var sourceArray = new int[5] { 1, 2, 3, 4, 5 };
            var comparableArray = new int[2] { 2, 3 };

            Assert.True(ArrayComparer.CompareArrays(sourceArray, comparableArray));
        }

        [Fact]
        public void Test2()
        {
            var sourceArray = new int[5] { 1, 2, 3, 4, 5 };
            var comparableArray = new int[3] { 1, 2, 4 };

            Assert.True(ArrayComparer.CompareArrays(sourceArray, comparableArray));
        }

        [Fact]
        public void Test3()
        {
            var sourceArray = new int[5] { 1, 2, 3, 4, 5 };
            var comparableArray = new int[2] { 2, 6 };

            Assert.False(ArrayComparer.CompareArrays(sourceArray, comparableArray));
        }

        [Fact]
        public void Test4()
        {
            var sourceArray = new[]{
                new[] { 1, 2, 3 },
                new[] { 3, 4, 5 },
                new[] { 7, 8, 9 }
            };

            var comparableArray = new[]{
                new[] { 3, 4, 5 },
                new[] { 7, 8, 9 }
            };

            Assert.True(ArrayComparer.CompareArrays(sourceArray, comparableArray));
        }

        [Fact]
        public void Test5()
        {
            var sourceArray = new[]{
                new[] { 1, 2, 3 },
                new[] { 3, 4, 5 },
                new[] { 7, 8, 9 }
            };

            var comparableArray = new[]{
                new[] { 3, 4, 5 }
            };

            Assert.True(ArrayComparer.CompareArrays(sourceArray, comparableArray));
        }

        [Fact]
        public void Test6()
        {
            var sourceArray = new[]{
                new[] { 1, 2, 3 },
                new[] { 3, 4, 5 },
                new[] { 7, 8, 9 }
            };

            var comparableArray = new[] { 3, 4, 5 };

            Assert.True(ArrayComparer.CompareArrays(sourceArray, comparableArray));
        }

        [Fact]
        public void Test7()
        {
            var sourceArray = new[]{
                new[] { 1, 2, 3 },
                new[] { 3, 4, 5 },
                new[] { 7, 8, 9 }
            };

            var comparableArray = new[]{
                new[] { 3, 4, 5 },
                new[] { 6, 7, 8 }
            };

            Assert.False(ArrayComparer.CompareArrays(sourceArray, comparableArray));
        }
    }
}
