﻿using System;
using System.Linq;

namespace Library
{
    public class ArrayComparer
    {
        public static bool CompareArrays(int[] sourceArray, int[] comparableArray)
        {
            return comparableArray.All(elem => sourceArray.Contains(elem));
        }

        public static bool CompareArrays(int[][] sourceArray, int[] comparableArray)
        {  
            return CompareArrays(sourceArray, new[] { comparableArray });            
        }

        public static bool CompareArrays(int[][] sourceArray, int[][] comparableArray)
        {
            var result = false;

            for (int i = 0; i < sourceArray.Length; i++)
            {
                if (result)
                {
                    break;
                }

                for (int j = 0; j < comparableArray.Length; j++)
                {
                    result = CompareArrays(sourceArray[i], comparableArray[j]);
                }
            }

            return result;
        }
    }
}
